project(nepomukindexer-plugins)

find_package(KDE4)
find_package(NepomukCore)

include(KDE4Defaults)

include_directories(
  ${QT_INCLUDES}
  ${KDE4_INCLUDES}
  ${SOPRANO_INCLUDE_DIR}
  ${NEPOMUK_CORE_INCLUDE_DIR}
  )

macro_optional_find_package(PopplerQt4)
macro_log_feature(POPPLER_QT4_FOUND "Poppler-Qt4" "A PDF rendering library" "http://poppler.freedesktop.org" FALSE "0.12.1" "Support for PDF files.")

macro_optional_find_package(Taglib)
macro_log_feature(TAGLIB_FOUND "Taglib" "Id3 tag reader" "taglib-website" FALSE "" "Support for music metadata")

macro_optional_find_package(Exiv2)
macro_log_feature(EXIV2_FOUND "Exiv2" "Image Tag reader" "http://www.exiv2.org" FALSE "" "Support for image metadata")

macro_optional_find_package(FFmpeg)
macro_log_feature(EXIV2_FOUND "FFmpeg" "Video Tag reader" "http://ffmpeg.org" FALSE "" "Support for video metadata")


# -----------------------------
# Extractors
#

if(POPPLER_QT4_FOUND)
    include_directories( ${POPPLER_QT4_INCLUDE_DIR} )

    kde4_add_plugin( nepomukpopplerextractor popplerextractor.cpp )

    target_link_libraries( nepomukpopplerextractor
        nepomukcore
        ${KDE4_KIO_LIBS}
        ${POPPLER_QT4_LIBRARIES}
    )

    install(
    FILES nepomukpopplerextractor.desktop
    DESTINATION ${SERVICES_INSTALL_DIR})

    install(
    TARGETS nepomukpopplerextractor
    DESTINATION ${PLUGIN_INSTALL_DIR})

endif(POPPLER_QT4_FOUND)

if(TAGLIB_FOUND)
    include_directories( ${TAGLIB_INCLUDES} )

    kde4_add_plugin( nepomuktaglibextractor taglibextractor.cpp )

    target_link_libraries( nepomuktaglibextractor
        nepomukcore
        ${KDE4_KIO_LIBS}
        ${TAGLIB_LIBRARIES}
    )

    install(
    FILES nepomuktaglibextractor.desktop
    DESTINATION ${SERVICES_INSTALL_DIR})

    install(
    TARGETS nepomuktaglibextractor
    DESTINATION ${PLUGIN_INSTALL_DIR})

endif(TAGLIB_FOUND)

if(EXIV2_FOUND)
    include_directories( ${EXIV2_INCLUDE_DIR} )

    kde4_add_plugin( nepomukexiv2extractor exiv2extractor.cpp )

    target_link_libraries( nepomukexiv2extractor
        nepomukcore
        ${KDE4_KIO_LIBS}
        ${EXIV2_LIBRARIES}
    )

    install(
    FILES nepomukexiv2extractor.desktop
    DESTINATION ${SERVICES_INSTALL_DIR})

    install(
    TARGETS nepomukexiv2extractor
    DESTINATION ${PLUGIN_INSTALL_DIR})

endif(EXIV2_FOUND)

if(FFMPEG_FOUND)
    include_directories( ${FFMPEG_INCLUDE_DIRS} )

    kde4_add_plugin( nepomukffmpegextractor ffmpegextractor.cpp )

    target_link_libraries( nepomukffmpegextractor
        nepomukcore
        ${KDE4_KIO_LIBS}
        ${FFMPEG_LIBRARIES}
    )

    install(
    FILES nepomukffmpegextractor.desktop
    DESTINATION ${SERVICES_INSTALL_DIR})

    install(
    TARGETS nepomukffmpegextractor
    DESTINATION ${PLUGIN_INSTALL_DIR})

endif(FFMPEG_FOUND)


kde4_add_plugin( nepomukplaintextextractor plaintextextractor.cpp )

target_link_libraries( nepomukplaintextextractor
    nepomukcore
    ${KDE4_KIO_LIBS}
    ${FFMPEG_LIBRARIES}
)

install(
FILES nepomukplaintextextractor.desktop
DESTINATION ${SERVICES_INSTALL_DIR})

install(
TARGETS nepomukplaintextextractor
DESTINATION ${PLUGIN_INSTALL_DIR})
